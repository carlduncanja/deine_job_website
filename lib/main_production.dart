import 'package:deine_job_website/app/app.dart';
import 'package:deine_job_website/bootstrap.dart';

void main() {
  bootstrap(() => const App());
}
